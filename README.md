# Blocklist Challenge

A coding challenge written against the FireHOL set of blocklists (see .gitmodules)
The goals were:
- to parse data from these lists
- to search a set of lists' IPv4 addresses for a given IPv4 address
- to search a set of lists' IPv4 CIDR ranges for a given IPv4 address

Limitations
- Not currently set to enter subfolders of the `blocklist-ipsets` directory
- No function for searching for a given IPv4 address across both CIDR ranges and individual addresses at this time (was not asked for)

This project uses pytest and poetry.
To run the tests, type: `poetry run pytest tests`
