import pytest
from pydantic import ValidationError

from blocklist_challenge.models import Blocklist, SearchResult


def test_Blocklist_valid_ips():
    """
    GIVEN a list of valid IPv4 address strings
    WHEN provided as 'ips' to a new Blocklist object
    THEN a valid Blocklist object should be created
    with the expected attributes
    """
    valid_ips = ["10.0.0.1", "192.168.0.1"]
    filename = "fake.ipset"

    blocklist = Blocklist(filename=filename, ips=valid_ips)
    assert blocklist.filename == "fake.ipset"
    assert sorted(blocklist.ips) == valid_ips


def test_Blocklist_invalid_ips():
    """
    GIVEN a list of strings containing an invalid IPv4 address
    WHEN provided as 'ips' to a new Blocklist object
    THEN a ValueError should result
    """
    ips_containing_bad_ip = ["1.1.1.1", "8.8.8.8", "900.657.40404.01"]
    filename = "fake.ipset"
    with pytest.raises(ValueError):
        Blocklist(filename=filename, ips=ips_containing_bad_ip)


def test_Blocklist_valid_cidrs():
    """
    GIVEN a list of valid IPv4 CIDR range strings
    WHEN provided as `cidrs` to a new Blocklist object
    THEN a valid Blocklist object should be created
    with the expected attributes
    """
    valid_cidrs = ["202.67.48.0/20", "216.52.244.32/28"]
    filename = "fake.ipset"

    blocklist = Blocklist(filename=filename, cidrs=valid_cidrs)
    assert blocklist.filename == "fake.ipset"
    assert sorted(blocklist.cidrs) == valid_cidrs


def test_Blocklist_invalid_cidrs():
    """
    GIVEN a list of strings containing an invalid IPv4 address
    WHEN provided as 'ips' to a new Blocklist object
    THEN a ValueError should result
    """
    cidrs_containing_invalid_cidr = ["74.125.227.0/29", "8.8.8.8", "10.0.0.0/24"]
    filename = "fake.netset"
    with pytest.raises(ValueError):
        Blocklist(filename=filename, ips=cidrs_containing_invalid_cidr)


def test_Blocklist_both_ips_and_cidrs_provided():
    """
    GIVEN valid values for `ips` and `cidrs`
    WHEN we attempt to instantiate a new Blocklist object
    THEN it should be successful
    """
    filename = "fake.netset"
    ips = ["15.204.9.251", "103.43.140.37"]
    cidrs = ["206.127.144.0/20", "209.120.141.240/28"]
    Blocklist(filename=filename, ips=ips, cidrs=cidrs)


def test_Blocklist_neither_ips_nor_cidrs_provided():
    """
    GIVEN no values for `cidrs` or `ips`
    WHEN we attempt to instantiate a new Blocklist object
    THEN a ValueError should result
    """
    filename = "fake.netset"
    with pytest.raises(ValidationError):
        Blocklist(filename=filename)


def test_Blocklist_no_filename_provided():
    """
    GIVEN no values for `cidrs` or `ips`
    WHEN we attempt to instantiate a new Blocklist object
    THEN a ValidationError should result
    """
    ips = ["15.204.9.251", "103.43.140.37"]
    cidrs = ["206.127.144.0/20", "209.120.141.240/28"]
    with pytest.raises(ValidationError):
        Blocklist(ips=ips, cidrs=cidrs)


def test_searchresult_valid_ipv4():
    """
    GIVEN a valid IPv4 address
    WHEN provided as 'searchedIP' to a new SearchResult object
    THEN a valid SearchResult object should be created
    with the expected attributes
    """
    valid_ipv4 = "8.8.8.8"

    result = SearchResult(searchedIP=valid_ipv4, found=True)
    assert result.searchedIP == valid_ipv4
    assert result.found


def test_searchresult_invalid_ipv4():
    """
    GIVEN an invalid IPv4 address
    WHEN provided as 'searchedIP' to a new SearchResult object
    THEN a ValueError should result
    """
    test_cidr_range = "134.73.173.216/31"
    found = True

    with pytest.raises(ValueError):
        SearchResult(searchedIP=test_cidr_range, found=found)
