import os
from pathlib import Path

import git
import pytest

from blocklist_challenge import utils

TEST_FILES_DIRECTORY = utils.get_repo_root() / "data" / "test"


def test_get_repo_root():
    """
    GIVEN a valid git repo was initialized for the project directory
    WHEN get_repo_root() is called
    THEN a valid Path object should be returned
    """
    root_path = utils.get_repo_root()
    assert isinstance(root_path, Path)
    assert root_path.exists


def test_get_repo_root_no_repo():
    """
    GIVEN a current directory outside of the git repo
    WHEN get_repo_root() is called
    THEN an error should be thrown
    """
    os.chdir("/")
    with pytest.raises(git.exc.InvalidGitRepositoryError):
        utils.get_repo_root()


def test_list_files_single_type():
    """
    GIVEN a test directory containing known files
    WHEN list_files() is called with a single extension type
    THEN it should return the expected filenames
    """
    filenames = utils.list_files(
        directory=TEST_FILES_DIRECTORY, file_extensions=[".pdf"]
    )
    sorted_filenames = sorted(filenames)
    assert sorted_filenames == ["MIT.pdf", "aws.pdf"]


def test_list_files_multiple_types():
    """
    GIVEN a test directory containing known files
    WHEN list_files() is called with multiple extension type
    THEN it should return the expected filenames
    """
    filenames = utils.list_files(
        directory=TEST_FILES_DIRECTORY, file_extensions=[".txt", ".png"]
    )
    sorted_filenames = sorted(filenames)
    assert sorted_filenames == ["README-merlin.txt", "key.png", "robots.txt"]


def test_valid_cidr_good_cidr():
    """
    GIVEN a valid CIDR range string
    WHEN valid_ip_or_cidr() is called
    THEN it should return True
    """
    good_cidr = "134.73.173.216/31"
    assert utils.valid_cidr(good_cidr)


def test_valid_cidr_not_a_cidr():
    """
    GIVEN a valid CIDR range string
    WHEN valid_ip_or_cidr() is called
    THEN it should return True
    """
    ipv4_address = "10.0.0.1"
    assert not utils.valid_cidr(ipv4_address)


def test_valid_ip_or_cidr_good_cidr():
    """
    GIVEN a valid CIDR range string
    WHEN valid_ip_or_cidr() is called
    THEN it should return True
    """
    good_cidr = "134.73.173.216/31"
    assert utils.valid_ip_or_cidr(good_cidr)


def test_valid_ip_or_cidr_bad_cidr():
    """
    GIVEN an invalid CIDR range string
    WHEN valid_ip_or_cidr() is called
    THEN it should return False
    """
    bad_cidr = "999.44.401.82/89"
    assert not utils.valid_ip_or_cidr(bad_cidr)
