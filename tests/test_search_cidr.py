"""Adapted from tests/test_search_ip for use with CIDR ranges"""

from blocklist_challenge import utils
from blocklist_challenge.models import BlockedResult, UnblockedResult
from blocklist_challenge.parse_task.parse_task import parse
from blocklist_challenge.search_cidr.search_cidr import search_cidr, count_duplicate_cidrs
from blocklist_challenge.utils import BLOCKLISTS_DIRECTORY_PATH


def test_search_ip_present_in_single_cidr_list():
    """
    GIVEN an IP address that we know is contained within one of the
    CIDR ranges of a blocklist file, and the blocklist file itself
    WHEN search_cidr() is called with the parameters
    THEN it should return a valid BlockedResult object
    with the expected attribute values
    """
    test_ip = "211.157.127.12"
    blocklist_name = "iblocklist_org_ubisoft.netset"
    data = parse(filenames=[blocklist_name])
    result = search_cidr(ip=test_ip, data=data)

    assert isinstance(result, BlockedResult)
    assert result.found
    assert result.lists == ["iblocklist_org_ubisoft.netset"]


def test_search_ip_absent_from_single_cidr_list():
    """
    GIVEN an IP address, and a blocklist file with CIDRs
    that we know do not contain it
    WHEN search_cidr() is called
    THEN it should return a valid UnblockedResult object
    """
    test_ip = "1.1.1.1"
    blocklist_name = "iblocklist_org_blizzard.netset"
    data = parse(filenames=[blocklist_name])
    result = search_cidr(ip=test_ip, data=data)

    assert isinstance(result, UnblockedResult)
    assert not result.found
    assert not hasattr(result, "lists")


def test_search_ip_present_in_multiple_cidr_lists():
    """
    GIVEN a set of blocklist files where more than one
    contain a particular CIDR range
    WHEN search_cidr() is called
    THEN it should return a valid BlockedResult object with
    more than one item in it's lists attribute
    """
    test_ip = "185.220.102.7"

    filenames = utils.list_files(
        directory=BLOCKLISTS_DIRECTORY_PATH, file_extensions=[".ipset", ".netset"]
    )
    data = parse(filenames=filenames)
    result = search_cidr(ip=test_ip, data=data)

    assert isinstance(result, BlockedResult)
    assert result.found
    assert len(result.lists) > 1


def test_search_ip_absent_from_multiple_cidr_lists():
    """
    GIVEN a known IP, and a set of blocklist files where
    none contain a CIDR range including the IP
    WHEN search_cidr() is called
    THEN it should return a valid UnblockedResult object
    with the expected attribute values
    """
    test_ip = "11.0.0.1"

    filenames = utils.list_files(
        directory=BLOCKLISTS_DIRECTORY_PATH, file_extensions=[".ipset", ".netset"]
    )
    data = parse(filenames=filenames)
    result = search_cidr(ip=test_ip, data=data)

    assert isinstance(result, UnblockedResult)
    assert not result.found
    assert not hasattr(result, "lists")


def test_count_duplicate_cidrs():
    """
    GIVEN a list of all blocklist files in the BLOCKLISTS_DIRECTORY
    WHEN count_duplicate_cidrs() is invoked
    THEN the result should be a list containing tuples, and none of
    the tuples should have a count (second index) less than 2
    """
    filenames = utils.list_files(
        directory=BLOCKLISTS_DIRECTORY_PATH, file_extensions=[".ipset", ".netset"]
    )
    results = count_duplicate_cidrs(filenames=filenames)
    assert isinstance(results, list)
    assert all([isinstance(item, tuple) for item in results])
    assert all([(count > 1) for (address, count) in results])
