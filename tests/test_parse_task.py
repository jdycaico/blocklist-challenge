from pathlib import Path

import pytest

from blocklist_challenge import utils
from blocklist_challenge.parse_task.parse_task import parse, read_file
from blocklist_challenge.utils import BLOCKLISTS_DIRECTORY_PATH

TEST_BLOCKLIST_FILENAME = "haley_ssh.ipset"


def test_read_file_valid_path():
    """
    GIVEN a valid file_path
    WHEN read_file() is called
    THEN a list of strings should be returned, containing neither
    comment lines nor leading/trailing whitespaces around the strings
    """
    file_path = Path(BLOCKLISTS_DIRECTORY_PATH) / TEST_BLOCKLIST_FILENAME

    file_contents = read_file(file_path)
    assert not any("#" in s for s in file_contents)
    assert not any((s.startswith(" ") or s.endswith(" ")) for s in file_contents)


def test_read_file_path_does_not_exist():
    """
    GIVEN a nonexistent file_path
    WHEN read_file() is called
    THEN an exception should be thrown with an explanatory message
    """
    fake_path = Path("/fake/path/myfile")
    with pytest.raises(FileNotFoundError):
        read_file(fake_path)


def test_read_file_invalid_file_type():
    """
    GIVEN a non-text file is provided for file_path
    WHEN read_file() is called
    THEN an exception should be thrown with an explanatory message
    """
    image_path = utils.get_repo_root() / "data" / "test" / "key.png"
    with pytest.raises(UnicodeDecodeError):
        read_file(image_path)


def test_parse_single_file():
    """
    GIVEN a valid FireHOL blocklist file
    WHEN parse() is called against its filename
    THEN a valid Blocklist object is returned
    """

    blocklist_file_name = "haley_ssh.ipset"
    blocklists = parse(filenames=[blocklist_file_name])

    assert len(blocklists) == 1
    haley_list = blocklists[0]

    assert isinstance(haley_list.filename, str)
    assert haley_list.filename == "haley_ssh.ipset"

    assert isinstance(haley_list.ips, list)
    assert all([isinstance(ip, str) for ip in haley_list.ips])
    assert all([utils.valid_ip_or_cidr(ip) for ip in haley_list.ips])


def test_parse_many_files():
    """
    GIVEN a directory of FireHOL blocklist files
    WHEN parse() is called against all the filenames
    THEN a list of valid Blocklist objects is returned
    """
    filenames = utils.list_files(
        directory=BLOCKLISTS_DIRECTORY_PATH, file_extensions=[".ipset", ".netset"]
    )

    blocklists = parse(filenames=filenames)

    assert len(blocklists) > 1

    assert all(isinstance(b.filename, str) for b in blocklists)
    assert all([b.filename in filenames for b in blocklists])

    assert all([isinstance(b.ips, list) for b in blocklists])
    for b in blocklists:
        assert all([isinstance(ip, str) for ip in b.ips])
        assert all([utils.valid_ip_or_cidr for ip in b.ips])
