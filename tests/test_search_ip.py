from blocklist_challenge import utils
from blocklist_challenge.models import BlockedResult, UnblockedResult
from blocklist_challenge.parse_task.parse_task import parse
from blocklist_challenge.search_ip.search_ip import count_duplicate_ips, search_ip
from blocklist_challenge.utils import BLOCKLISTS_DIRECTORY_PATH


def test_search_ip_present_in_single_list():
    """
    GIVEN a blocklist file containing a known IP address
    WHEN search_ip() is called
    THEN it should return a valid BlockedResult object
    """
    test_ip = "12.28.86.162"
    blocklist_name = "haley_ssh.ipset"
    data = parse(filenames=[blocklist_name])
    result = search_ip(ip=test_ip, data=data)

    assert isinstance(result, BlockedResult)
    assert result.found
    assert result.lists == ["haley_ssh.ipset"]


def test_search_ip_absent_from_single_list():
    """
    GIVEN an IP address, and a blocklist file we know does not contain it
    WHEN search_ip() is called
    THEN it should return a valid UnblockedResult object
    """
    test_ip = "1.1.1.1"
    blocklist_name = "proxz.ipset"
    data = parse(filenames=[blocklist_name])
    result = search_ip(ip=test_ip, data=data)

    assert isinstance(result, UnblockedResult)
    assert not result.found
    assert not hasattr(result, "lists")


def test_search_ip_present_in_multiple_lists():
    """
    GIVEN a set of blocklist files where more than one
    contain a particular IP address
    WHEN search_ip() is called
    THEN it should return a valid BlockedResult object with
    more than one item in it's lists attribute
    """
    test_ip = "109.86.234.51"

    filenames = utils.list_files(
        directory=BLOCKLISTS_DIRECTORY_PATH, file_extensions=[".ipset"]
    )
    data = parse(filenames=filenames)
    result = search_ip(ip=test_ip, data=data)

    assert isinstance(result, BlockedResult)
    assert result.found
    assert len(result.lists) > 1


def test_search_ip_absent_from_multiple_lists():
    """
    GIVEN a set of blocklist files where none contain a known address
    WHEN search_ip() is called
    THEN it should return a valid UnblockedResult object with the
    expected attribute values
    """
    test_ip = "10.0.0.1"

    filenames = utils.list_files(
        directory=BLOCKLISTS_DIRECTORY_PATH, file_extensions=[".ipset"]
    )
    data = parse(filenames=filenames)
    result = search_ip(ip=test_ip, data=data)

    assert isinstance(result, UnblockedResult)
    assert not result.found
    assert not hasattr(result, "lists")


def test_count_duplicate_ips_ipsets():
    """
    GIVEN a list of all .ipset files in the BLOCKLISTS_DIRECTORY
    WHEN count_duplicate_ips() is invoked
    THEN the result should be a list containing tuples, and none of
    the tuples should have a count (second index) less than 2
    """
    filenames = utils.list_files(
        directory=BLOCKLISTS_DIRECTORY_PATH, file_extensions=[".ipset"]
    )
    results = count_duplicate_ips(filenames=filenames)
    assert isinstance(results, list)
    assert all([isinstance(item, tuple) for item in results])
    assert all([(count > 1) for (address, count) in results])
