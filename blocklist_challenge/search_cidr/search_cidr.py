from collections import Counter
from typing import List

from netaddr import IPNetwork

from blocklist_challenge.models import (
    BlockedResult,
    Blocklist,
    SearchResult,
    UnblockedResult,
)
from blocklist_challenge.parse_task.parse_task import parse


def search_cidr(ip: str, data: List[Blocklist]) -> SearchResult:
    """
    Searches a list of Blocklist objects' `cidrs` attributes
    for the presence of the provided IP address
    Will only match on CIDR ranges in the input data, not IPs

    A small modification of the search_ip folder's search_ip() function

    :param ip: an IPv4 address we want to search for
    :param data: a list of Blocklist objects with
    the `cidrs` attribute present and populated
    :return: a BlockedResult or UnblockedResult object
    """
    match_found = False
    blocklist_matches = []
    for blocklist in data:
        for cidr in blocklist.cidrs:
            if ip in IPNetwork(cidr):
                match_found = True
                blocklist_matches.append(blocklist.filename)

    if match_found:
        return BlockedResult(searchedIP=ip, lists=blocklist_matches)
    else:
        return UnblockedResult(searchedIP=ip)
    pass


def count_duplicate_cidrs(filenames: list) -> List[tuple]:
    """
    Takes a list of filenames, parses CIDR ranges from those files, and counts
    the number of times each range is duplicated across the set of files

    :param filenames: a list of filenames to read for collecting addresses
    :return: counts_exceeding_one: a list of tuples of format (address, count),
    ordered by count, where count is greater than 1
    """
    blocklists = parse(filenames=filenames)
    all_cidrs = []
    for b in blocklists:
        for cidr in b.cidrs:
            all_cidrs.append(cidr)

    all_counts = Counter(all_cidrs).most_common()

    counts_exceeding_one = [c for c in all_counts if c[1] > 1]
    return counts_exceeding_one
