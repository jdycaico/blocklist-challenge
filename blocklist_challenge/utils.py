from pathlib import Path
from typing import List

import git
import netaddr


def get_repo_root() -> Path:
    """
    Utility function used to get the root path of the current repository
    For use in building absolute paths in a consistent manner

    :return root_path: the path to the top-most directory
    managed by the git repository
    """
    repo = git.Repo()
    root_directory = repo.git.rev_parse("--show-toplevel")
    root_path = Path(root_directory)
    return root_path


BLOCKLISTS_DIRECTORY_PATH = get_repo_root() / "data" / "blocklist-ipsets"


def list_files(directory: Path, file_extensions: List[str]) -> List[str]:
    """
    Uses pathlib's glob() to find all files that are direct children of the
    directory path we provide. Filters results down against our desired set of
    file extensions, then returns a list of the filenames

    :param directory: the path of the directory for which we want to list files
    :param file_extensions: the set of file extensions we want to filter
    our file listing results to
    """
    directory_contents = directory.glob("*")  # '**/*' for recursive glob
    directory_files = [p for p in directory_contents if p.is_file()]
    target_files = [p for p in directory_files if p.suffix in file_extensions]
    filenames = [p.name for p in target_files]
    return filenames


def valid_cidr(address: str) -> bool:
    """
    Validation function for strings to determine if they are a valid CIDR range
    according to the netaddr library

    :param address: a string that we desire to validate
    :return: a boolean indicating whether or not the string is
    a valid CIDR range
    """
    if netaddr.valid_ipv4(address):
        return False
    else:
        try:
            netaddr.IPNetwork(address)
        except netaddr.core.AddrFormatError:
            return False
    return True


def valid_ip_or_cidr(address: str) -> bool:
    """
    Validation function for strings to determine if they are either
    valid IPv4 or a valid CIDR range

    :param address: a string that we desire to validate
    :return: a boolean indicating whether or not the string is either a valid
    IPv4 address or CIDR range
    """
    if netaddr.valid_ipv4(address):
        return True
    else:
        return valid_cidr(address)
