from pathlib import Path
from typing import List

from netaddr import valid_ipv4

from blocklist_challenge.models import Blocklist
from blocklist_challenge.utils import BLOCKLISTS_DIRECTORY_PATH, valid_cidr


def read_file(file_path: Path) -> List[str]:
    """
    Reads the file at the provided path line-by-line into a list of strings
    Removes lines containing a pound character
    Removes whitespace around each line

    :param file_path: a pathlib Path to a file
    :return: a list of strings, one for each line of the file
    """

    with open(file_path) as input_file:
        file_lines = input_file.readlines()

    file_lines_no_comments = [s for s in file_lines if "#" not in s]
    file_lines_no_whitespace = [s.strip() for s in file_lines_no_comments]

    if len(file_lines_no_whitespace) == 0:
        return None
    else:
        return file_lines_no_whitespace


def parse(filenames: List[str]) -> List[object]:
    """
    Parses a set of FireHOL blocklist files by filename
    containing either single IP addresses or CIDR ranges
    into a list of Blocklist objects

    :param filenames: a list of filenames to parse
    :return: blocklists: a list of firehol_data objects
    """
    assert isinstance(filenames, list)

    blocklists = []
    for filename in filenames:
        file_path = BLOCKLISTS_DIRECTORY_PATH / filename
        file_data = read_file(file_path)
        if file_data:
            ips = []
            cidrs = []
            for line in file_data:
                if valid_ipv4(line):
                    ips.append(line)
                elif valid_cidr(line):
                    cidrs.append(line)
            this_blocklist = Blocklist(filename=filename, ips=ips, cidrs=cidrs)
            blocklists.append(this_blocklist)
    return blocklists
