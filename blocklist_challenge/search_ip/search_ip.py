from collections import Counter
from typing import List

from blocklist_challenge.models import BlockedResult, UnblockedResult
from blocklist_challenge.parse_task.parse_task import parse


def search_ip(ip: str, data: List[object]) -> object:
    """
    Searches the provided list of Blocklist blocklist objects
    for the presence of the provided IP address
    Will only match on IPv4 addresses in the input data, not CIDR ranges

    :param ip: a string representation of an IP address
    :param data: a list of Blocklist objects
    :return: a SearchResult object denoting whether or not the
    IP was found in the input data, and if so, in which file
    """
    match_found = False
    blocklist_matches = []
    for blocklist in data:
        if ip in blocklist.ips:
            match_found = True
            blocklist_matches.append(blocklist.filename)

    if match_found:
        return BlockedResult(searchedIP=ip, lists=blocklist_matches)
    else:
        return UnblockedResult(searchedIP=ip)


def count_duplicate_ips(filenames: list) -> List[tuple]:
    """
    Takes a list of filenames, parses addresses from those files, and counts
    the number of times each address is duplicated across the set of files

    :param filenames: a list of filenames to read for collecting addresses
    :return: counts_exceeding_one: a list of tuples of format (address, count),
    ordered by count, where count is greater than 1
    """
    blocklists = parse(filenames=filenames)
    all_ips = []
    for b in blocklists:
        for ip in b.ips:
            all_ips.append(ip)

    all_counts = Counter(all_ips).most_common()

    counts_exceeding_one = [c for c in all_counts if c[1] > 1]
    return counts_exceeding_one
