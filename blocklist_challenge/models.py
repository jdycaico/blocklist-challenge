from typing import List, Optional

from netaddr import valid_ipv4
from pydantic import BaseModel, field_validator, model_validator

from blocklist_challenge.utils import valid_cidr


class Blocklist(BaseModel):
    """Holds the data contained within a single FireHOL blocklist file.

    Instances of this object are sometimes referred to by the variable name
    'blocklist' in the code
    Supports the loading of both .ipset and .netset files

    Attributes:
        filename: A string filename indicating from which blocklist file the
        object was generated
        ips: A list of string IPv4 addresses contained within the file
        cidrs: A list of string IPv4 CIDR ranges contained within the file
    """

    filename: str
    ips: Optional[list] = None
    cidrs: Optional[list] = None

    @field_validator("ips")
    def validate_ips_if_present(cls, ips: list, filename: str):
        if ips is not None:
            if not all([valid_ipv4(ip) for ip in ips]):
                raise ValueError(
                    f"""Invalid IPs provided to Blocklist object
                                    Check filename:{filename}"""
                )
            return ips

    @field_validator("cidrs")
    def validate_cidrs_if_present(cls, cidrs: list, filename: str):
        if cidrs is not None:
            if not all([valid_cidr(c) for c in cidrs]):
                raise ValueError(
                    f"""Invalid CIDRs provided to Blocklist object
                                    Check filename:{filename}"""
                )
            return cidrs

    @model_validator(mode="after")
    def must_have_cidrs_or_ips(self):
        if not self.ips and not self.cidrs:
            raise ValueError(
                """Blocklist objects require either cidrs or ips
                            to be provided upon creation"""
            )
        return self


class SearchResult(BaseModel):
    """Holds the information returned from execution of the search_ip() function.

    Attributes:
        searchedIP: The string address that was provided to search_ip()
        found: A boolean value indicating the presence or absence of the
        address in the set of Blocklist object provided to search_ip()
    """

    searchedIP: str
    found: bool

    @field_validator("searchedIP")
    def ip_must_be_valid(cls, searchedIP: str):
        if not valid_ipv4(searchedIP):
            raise ValueError(
                f"""Invalid IP provided to SearchResult object
                                searchedIP:{searchedIP}"""
            )
        return searchedIP


class UnblockedResult(SearchResult):
    """Subclass of SearchResult used in cases when no matches are found.

    Attributes:
        found: A boolean hardcoded to False, indicating no match was found
    """

    found: bool = False


class BlockedResult(SearchResult):
    """Subclass of SearchResult used in cases when matches are found.

    The name indicates that a blocking action should be taken for the
    searchedIP, but that is oustide the scope of this challenge

    Attributes:
        found: A boolean hardcoded to True, indicating that a match was found
        lists: A list of strings, consisting of the filenames of the blocklist
        files in which the address was found
    """

    found: bool = True
    lists: List[str]
